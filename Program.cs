﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Collections.Specialized;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            using (var client = new WebClient())
            {
                // Создаем запрос(формируем строку запроса

                long OrderId = rnd.Next();// номер заказа
                string OrderIdStr = OrderId.ToString();
                string PayInfo = "PAN=4111111111111112;EMonth=4;EYear=19;CardHolder=Ivan Ivanov;SecureCode=123;OrderId=" + OrderIdStr + "; Amount=100;";
                var values = new NameValueCollection();
                values["Key"] = "Merchant";
                values["Amount"] = "100";
                values["OrderId"] = OrderIdStr;
                values["PayInfo"] = HttpUtility.UrlEncode(PayInfo);// перевод в  Url encoded строку

                var response = client.UploadValues("https://sandbox2.payture.com/api/Pay", values);// запрос запрос к тестовому сервису
                var responseString = Encoding.Default.GetString(response);// получаем ответ от сервера в формате XMl строки

                // Обработка ответа(проведем XML сериализацию)
                Type t = typeof(Pay);
                var ser = new XmlSerializer(t);
                XmlReader reader = XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(responseString ?? "")));
                var ans = (Pay)ser.Deserialize(reader);

                // Выведем сообщение операции на консоль
                if (ans.Success == "True")
                {
                    Console.WriteLine("Операция была проведена успешна\nНомер заказа: " + ans.OrderId + "\nСчет: " + ans.Amount + "\nИдентификатор ТСП: " + ans.Key);
                }
                else
                {
                    Console.WriteLine("Операция не успешна\n");
                    switch (ans.ErrCode)
                    {
                        case "ACCESS_DENIED":
                            Console.WriteLine("Доступ с текущего IP или по указанным параметрам запрещен");
                            break;
                        case "DUPLICATE_ORDER_ID":
                            Console.WriteLine("Номер заказа уже использовался ранее");
                            break;
                        case "AMOUNT_ERROR":
                            Console.WriteLine("Неверно указана сумма транзакции");
                            break;
                        case "FORMAT_ERROR":
                            Console.WriteLine("Неверный формат переданных данных");
                            break;
                        default:
                            break;
                    }
                }

            }
            Console.ReadKey();
        }
    }


    [Serializable]
    public class Pay
    {
        [XmlAttribute("Success")]
        public string Success { get; set; }

        [XmlAttribute("Key")]
        public string Key { get; set; }

        [XmlAttribute("OrderId")]
        public string OrderId { get; set; }

        [XmlAttribute("Amount")]
        public string Amount { get; set; }

        [XmlAttribute("ErrCode")]
        public string ErrCode { get; set; }
    }
}
